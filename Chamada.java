package teste;

import java.util.ArrayList;
import java.util.List;

public class Chamada {

    private String nomeAluno;
    private List<Presenca> presencasAluno;

    public Chamada() {
        this.presencasAluno = new ArrayList<>();
    }

    public String getNomeAluno() {
        return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
        this.nomeAluno = nomeAluno;
    }

    public List<Presenca> getPresencasAluno() {
        return presencasAluno;
    }

    public void setPresencasAluno(List<Presenca> presencasAluno) {
        this.presencasAluno = presencasAluno;
    }
}
