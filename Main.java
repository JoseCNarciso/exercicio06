package teste;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        List<Chamada> presencaDB = new ArrayList<>();

        int opc = -1;
        do {
            System.out.println("Digite uma opção:");
            System.out.println("1 - Cadastrar aluno");
            System.out.println("2 - Listar alunos");
            System.out.println("3 - Registrar presenca");
            System.out.println("0 - Sair");
            opc = scanner.nextInt();

            switch (opc) {
                case 1:
                    System.out.println("Digite o nome do aluno: ");
                    String nome = scanner.next();
                    Chamada aluno = new Chamada();
                    aluno.setNomeAluno(nome);

                    presencaDB.add(aluno);
                    break;
                case 2:

                    System.out.println("==================================== ");

                    for (int i = 0; i < presencaDB.size(); i++) {
                        System.out.println(presencaDB.get(i).getNomeAluno());
                        System.out.println("presenças: ");

                        for(Presenca presenca : presencaDB.get(i).getPresencasAluno()) {
                            System.out.print(presenca.getData());
                            System.out.print("=");
                            System.out.println(presenca.isPresente());
                        }
                    }

                    System.out.println("==================================== ");

                    break;
                case 3:

                    for (Chamada aluno1 : presencaDB) {
                        System.out.print("Deseja registrar a presenca do ");
                        System.out.print(aluno1.getNomeAluno());
                        System.out.println("? (S/N)");

                        String opcaoPresenca = scanner.next();

                        if (opcaoPresenca.equalsIgnoreCase("S")) {
                            LocalDate data = LocalDate.now();
                            System.out.print("O aluno esteve presente na data ");
                            System.out.print(data.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                            System.out.print("? (S/N)");

                            String resposta = scanner.next();
                            boolean presente = false;
                            if (resposta.equalsIgnoreCase("S")) {
                                presente = true;
                            } else {
                                presente = false;
                            }

                            Presenca presenca = new Presenca();
                            presenca.setData(data);
                            presenca.setPresente(presente);

                            aluno1.getPresencasAluno().add(presenca);
                        }
                    }
                    break;
            }
        } while (opc != 0);
    }
}
